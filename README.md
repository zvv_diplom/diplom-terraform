# diplom-terraform

## Создания кластера

Для создания кластера необходимо установить ПО:

# Yandex.Cloud (CLI)

```
$ curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
```

Зарегистрироваться в Яндех.Cloud
Инициировать токены доступа:
```
export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)
```
# ansible
```
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo add-apt-repository --yes --update ppa:ansible/ansible
$ sudo apt install ansible
```

# kubectl

```
curl -LO https://dl.k8s.io/release/`curl -LS https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

# terraform

```
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
```

Коздать ключи ssh:

```
ssh-keygen
```

После этого запустить скрипт установки кластера:

```
./create_infra.sh
```
В результате работы скрипта будет создано:
    - два сервера в одном кластере Kubernetes: 1 master и 1 worker;
    - сервер srv для инструментов мониторинга, логгирования и сборок контейнеров.

  