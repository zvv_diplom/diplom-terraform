#!/bin/bash

set -e

TF_IN_AUTOMATION=1 terraform init
TF_IN_AUTOMATION=1 terraform apply -auto-approve

export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i kubespray/inventory/hosts.ini --become wait_server_starts.yml

cd kubespray
ansible-playbook -i inventory/hosts.ini --become cluster.yml
