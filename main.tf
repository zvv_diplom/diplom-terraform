terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~>0.65.0"
    }
  }
}
provider "yandex" {
   zone = "ru-central1-a"
   folder_id = "b1gd051vbt3j7qhajh11"
#   service_account_key_file = "key.json"

}

data "yandex_compute_image" "ubuntu-image" {
  family = "ubuntu-2204-lts"
}


resource "yandex_vpc_network" "diplom-network" {
  name = "diplom-network"
}

resource "yandex_vpc_subnet" "subnet" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.diplom-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_iam_service_account" "diplom-account" {
 name        = "diplom-account"
 description = "Сервисный аккаунт zvv"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
 # Сервисному аккаунту назначается роль "editor".
 folder_id = "b1gd051vbt3j7qhajh11"
 role      = "editor"
 member    = "serviceAccount:${yandex_iam_service_account.diplom-account.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
 # Сервисному аккаунту назначается роль "container-registry.images.puller".
 folder_id = "b1gd051vbt3j7qhajh11"
 role      = "container-registry.images.puller"
 member    = "serviceAccount:${yandex_iam_service_account.diplom-account.id}"
}

resource "yandex_iam_service_account_static_access_key" "static-access-key" {
  service_account_id = yandex_iam_service_account.diplom-account.id
  depends_on = [
    yandex_iam_service_account.diplom-account
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = "b1gd051vbt3j7qhajh11"
  role = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.diplom-account.id}",
  ]
  depends_on = [
    yandex_iam_service_account.diplom-account,
  ]
}



resource "yandex_compute_instance_group" "diplom-masters" {
  name               = "diplom-masters"
  service_account_id = yandex_iam_service_account.diplom-account.id
  depends_on = [
    yandex_iam_service_account.diplom-account,
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_vpc_network.diplom-network
  ]
  
  # Шаблон экземпляра, к которому принадлежит группа экземпляров.
  instance_template {

    # Имя виртуальных машин, создаваемых Instance Groups
    name = "master-{instance.index}"

    # Ресурсы, которые будут выделены для создания виртуальных машин в Instance Groups
    resources {
      cores  = 2
      memory = 2
      core_fraction = 20 # Базовый уровень производительности vCPU. https://cloud.yandex.ru/docs/compute/concepts/performance-levels
    }

    # Загрузочный диск в виртуальных машинах в Instance Groups
    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.ubuntu-image.id
        size     = 10
        type     = "network-ssd"
      }
    }

    network_interface {
      network_id = yandex_vpc_network.diplom-network.id
      subnet_ids = [
        yandex_vpc_subnet.subnet.id
      ]
      # Флаг nat true указывает что виртуалкам будет предоставлен публичный IP адрес.
      nat = true
    }

    metadata = {
        user-data = templatefile("${path.module}/meta.yaml", {
            vm_user      = "zvv"
            ssh_key_path = file("~/.ssh/id_rsa_mo.pub")
        })
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = [
      "ru-central1-a",
    ]
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 1
    max_deleting    = 1
  }
}


resource "yandex_compute_instance_group" "diplom-workers" {
  name               = "diplom-workers"
  service_account_id = yandex_iam_service_account.diplom-account.id
  depends_on = [
    yandex_iam_service_account.diplom-account,
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_vpc_network.diplom-network
  ]

  instance_template {

    name = "worker-{instance.index}"

    resources {
      cores  = 2
      memory = 2
      core_fraction = 20
    }

    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.ubuntu-image.id
        size     = 10
        type     = "network-hdd"
      }
    }

    network_interface {
      network_id = yandex_vpc_network.diplom-network.id
      subnet_ids = [
        yandex_vpc_subnet.subnet.id
      ]
      # Флаг nat true указывает что виртуалкам будет предоставлен публичный IP адрес.
      nat = true
    }

    metadata = {
        user-data = templatefile("${path.module}/meta.yaml", {
            vm_user      = "zvv"
            ssh_key_path = file("~/.ssh/id_rsa_mo.pub")
        })
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = [
      "ru-central1-a"
    ]
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 1
    max_deleting    = 1
  }
}



data "template_file" "inventory" {
  template = file("inventory.tpl")
  vars = {
    master_1_public_ip = yandex_compute_instance_group.diplom-masters.instances.*.network_interface.0.nat_ip_address[0]
    master_1_private_ip = yandex_compute_instance_group.diplom-masters.instances.*.network_interface.0.ip_address[0]
    worker_1_public_ip = yandex_compute_instance_group.diplom-workers.instances.*.network_interface.0.nat_ip_address[0]
    worker_1_private_ip = yandex_compute_instance_group.diplom-workers.instances.*.network_interface.0.ip_address[0]
    key_path = "~/.ssh/id_rsa_mo"
  }
}

resource "null_resource" "update_inventory" {
    provisioner "local-exec" {
        command = "echo '${data.template_file.inventory.rendered}' > kubespray/inventory/hosts.ini"
    }
}




resource "yandex_compute_instance" "srv" {
    name = "srv"
    hostname = "srv"
    resources {
      cores = 2
      memory = 4
      core_fraction = 20
    }
    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.ubuntu-image.id
        size = 16
      }
    }
    network_interface {
      subnet_id = yandex_vpc_subnet.subnet.id
      nat = true
    }
    metadata = {
        user-data = templatefile("${path.module}/meta.yaml", {
            vm_user      = "zvv"
            ssh_key_path = file("~/.ssh/id_rsa_mo.pub")
        })
    }

    connection {
        type = "ssh"
        user = "zvv"
        private_key = file("~/.ssh/id_rsa_mo")
        host = self.network_interface[0].nat_ip_address
    }
    provisioner "remote-exec" {
        inline = [
<<EOT
            # Add Docker's official GPG key:
            sudo apt-get update -y
            sudo apt-get install -y ca-certificates curl git
            sudo install -m 0755 -d /etc/apt/keyrings
            sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
            sudo chmod a+r /etc/apt/keyrings/docker.asc
            # Add the repository to Apt sources:
            echo \
              "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
              $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
              sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            sudo apt-get update
            sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
            sudo usemod -aG docker zvv
            curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"
            sudo dpkg -i gitlab-runner_amd64.deb

EOT                
        ]            
    }
  
}


output "instance_group_masters_public_ips" {
  description = "Public IP addresses for master-nodes"
  value = yandex_compute_instance_group.diplom-masters.instances.*.network_interface.0.nat_ip_address
}

output "instance_group_masters_private_ips" {
  description = "Private IP addresses for master-nodes"
  value = yandex_compute_instance_group.diplom-masters.instances.*.network_interface.0.ip_address
}

output "instance_group_workers_public_ips" {
  description = "Public IP addresses for worder-nodes"
  value = yandex_compute_instance_group.diplom-workers.instances.*.network_interface.0.nat_ip_address
}

output "instance_group_workers_private_ips" {
  description = "Private IP addresses for worker-nodes"
  value = yandex_compute_instance_group.diplom-workers.instances.*.network_interface.0.ip_address
}


output "static-key-access-key" {
  description = "Access key for admin user"
  value = yandex_iam_service_account_static_access_key.static-access-key.access_key
}

output "static-key-secret-key" {
  description = "Secret key for admin user"
  value = yandex_iam_service_account_static_access_key.static-access-key.secret_key
  sensitive = true
}
