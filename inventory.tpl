[all:vars]
master-1 ansible_host=${master_1_public_ip} ip=${master_1_private_ip} etcd_member_name=etcd1
worker-1 ansible_host=${worker_1_public_ip} ip=${worker_1_private_ip} etcd_member_name=etcd1

ansible_ssh_user = zvv
ansible_user = zvv
ansible_ssh_private_key_file = ${key_path}

[kube-master]
master-1

[etcd]
master-1

[kube-node]
worker-1

[kube-worker]
worker-1

[kube-ingress]
worker-1

[k8s-cluster:children]
kube-master
kube-node